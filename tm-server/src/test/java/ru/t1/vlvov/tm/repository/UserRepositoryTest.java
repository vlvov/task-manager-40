package ru.t1.vlvov.tm.repository;

import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.vlvov.tm.api.repository.IUserRepository;
import ru.t1.vlvov.tm.api.service.IConnectionService;
import ru.t1.vlvov.tm.api.service.IPropertyService;
import ru.t1.vlvov.tm.enumerated.Role;
import ru.t1.vlvov.tm.marker.UnitCategory;
import ru.t1.vlvov.tm.model.User;
import ru.t1.vlvov.tm.service.ConnectionService;
import ru.t1.vlvov.tm.service.PropertyService;
import ru.t1.vlvov.tm.util.HashUtil;

import static ru.t1.vlvov.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public final class UserRepositoryTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @After
    @SneakyThrows
    public void tearDown() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
            repository.clearUser();
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Test
    @SneakyThrows
    public void add() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
            repository.addUser(USER1);
            sqlSession.commit();
            Assert.assertEquals(USER1.getId(), repository.findUserById(USER1.getId()).getId());
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Test
    @SneakyThrows
    public void clear() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
            repository.addUser(USER1);
            repository.addUser(USER2);
            sqlSession.commit();
            repository.clearUser();
            sqlSession.commit();
            Assert.assertNull(repository.findUserById(USER1.getId()));
            Assert.assertNull(repository.findUserById(USER2.getId()));
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Test
    @SneakyThrows
    public void findAll() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
            for (final User user : USER_LIST) {
                repository.addUser(user);
            }
            sqlSession.commit();
            Assert.assertFalse(repository.findAllUser().isEmpty());
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Test
    @SneakyThrows
    public void findOneById() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
            repository.addUser(USER1);
            sqlSession.commit();
            Assert.assertEquals(USER1.getId(), repository.findUserById(USER1.getId()).getId());
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Test
    @SneakyThrows
    public void remove() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
            repository.addUser(USER1);
            sqlSession.commit();
            Assert.assertEquals(USER1.getId(), repository.findUserById(USER1.getId()).getId());
            repository.removeUser(USER1);
            sqlSession.commit();
            Assert.assertNull(repository.findUserById(USER1.getId()));
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Test
    @SneakyThrows
    public void removeById() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
            repository.addUser(USER1);
            sqlSession.commit();
            Assert.assertEquals(USER1.getId(), repository.findUserById(USER1.getId()).getId());
            repository.removeUserById(USER1.getId());
            sqlSession.commit();
            Assert.assertNull(repository.findUserById(USER1.getId()));
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Test
    @SneakyThrows
    public void existsById() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
            repository.addUser(USER1);
            sqlSession.commit();
            Assert.assertNotNull(repository.findUserById(USER1.getId()));
            repository.removeUserById(USER1.getId());
            sqlSession.commit();
            Assert.assertNull(repository.findUserById(USER1.getId()));
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Test
    @SneakyThrows
    public void createLoginPassword() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
            Assert.assertNull(repository.findByLogin("USER1_LOGIN"));
            @NotNull final User user = new User();
            user.setLogin("USER1_LOGIN");
            user.setPasswordHash(HashUtil.salt("USER1_PASSWORD", propertyService));
            repository.addUser(user);
            sqlSession.commit();
            Assert.assertNotNull(repository.findByLogin("USER1_LOGIN"));
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Test
    @SneakyThrows
    public void createLoginPasswordEmail() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
            Assert.assertNull(repository.findByLogin("USER1_LOGIN"));
            @NotNull final User user = new User();
            user.setLogin("USER1_LOGIN");
            user.setPasswordHash(HashUtil.salt("USER1_PASSWORD", propertyService));
            user.setEmail("USER1_EMAIL");
            repository.addUser(user);
            sqlSession.commit();
            Assert.assertNotNull(repository.findByLogin("USER1_LOGIN"));
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Test
    @SneakyThrows
    public void createLoginPasswordRole() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
            Assert.assertNull(repository.findByLogin("USER1_LOGIN"));
            @NotNull final User user = new User();
            user.setLogin("USER1_LOGIN");
            user.setPasswordHash(HashUtil.salt("USER1_PASSWORD", propertyService));
            user.setRole(Role.ADMIN);
            repository.addUser(user);
            sqlSession.commit();
            Assert.assertNotNull(repository.findByLogin("USER1_LOGIN"));
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Test
    @SneakyThrows
    public void findByLogin() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
            @NotNull final User user = new User();
            user.setLogin("USER1_LOGIN");
            user.setPasswordHash(HashUtil.salt("USER1_PASSWORD", propertyService));
            repository.addUser(user);
            sqlSession.commit();
            Assert.assertEquals(user.getId(), repository.findByLogin(user.getLogin()).getId());
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Test
    @SneakyThrows
    public void findByEmail() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
            @NotNull final User user = new User();
            user.setLogin("USER1_LOGIN");
            user.setPasswordHash(HashUtil.salt("USER1_PASSWORD", propertyService));
            user.setEmail("USER1_EMAIL");
            repository.addUser(user);
            sqlSession.commit();
            Assert.assertEquals(user.getId(), repository.findByEmail(user.getEmail()).getId());
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

}
