package ru.t1.vlvov.tm.api.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.vlvov.tm.enumerated.Status;
import ru.t1.vlvov.tm.model.Project;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IProjectService {

    @Nullable
    Project changeProjectStatusById(@Nullable String userId,
                                    @Nullable String id,
                                    @NotNull Status status);


    @Nullable
    Project create(@Nullable String userId, @Nullable String name);

    @Nullable
    Project create(@Nullable String userId, @Nullable String name, @NotNull String description);

    @Nullable
    Project updateById(@Nullable String userId,
                       @Nullable String id,
                       @Nullable String name,
                       @NotNull String description);

    @NotNull
    @SneakyThrows
    Project add(@Nullable Project model);

    @NotNull
    @SneakyThrows
    Collection<Project> add(@Nullable Collection<Project> projects);

    @NotNull
    @SneakyThrows
    Collection<Project> set(@Nullable Collection<Project> models);

    @SneakyThrows
    void clear();

    @NotNull
    @SneakyThrows
    List<Project> findAll();

    @NotNull
    @SneakyThrows
    Project remove(@Nullable Project project);

    @Nullable
    @SneakyThrows
    Project removeById(@Nullable String id);

    @Nullable
    @SneakyThrows
    Project findOneById(@Nullable String id);

    @Nullable
    @SneakyThrows
    List<Project> findAll(@Nullable Comparator comparator);

    @SneakyThrows
    boolean existsById(@Nullable String id);

    @NotNull
    @SneakyThrows
    Project add(@Nullable String userId, @Nullable Project project);

    @SneakyThrows
    void clear(@Nullable String userId);

    @Nullable
    @SneakyThrows
    List<Project> findAll(@Nullable String userId);

    @Nullable
    @SneakyThrows
    Project findOneById(@Nullable String userId, @Nullable String id);

    @Nullable
    @SneakyThrows
    Project remove(@Nullable String userId, @Nullable Project project);

    @Nullable
    @SneakyThrows
    Project removeById(@Nullable String userId, @Nullable String id);

    @Nullable
    @SneakyThrows
    List<Project> findAll(@Nullable String userId, @Nullable Comparator comparator);

    @SneakyThrows
    boolean existsById(@Nullable String userId, @Nullable String id);
}
