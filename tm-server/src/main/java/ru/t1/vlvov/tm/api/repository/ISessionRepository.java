package ru.t1.vlvov.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.vlvov.tm.model.Session;

import java.util.List;

public interface ISessionRepository {

    @Insert("INSERT INTO SESSION (ID, CREATED, ROLE, USER_ID)"
            + "VALUES (#{id}, #{created}, #{role}, #{userId})")
    void addSession(@NotNull Session session);

    @Delete("DELETE FROM SESSION WHERE USER_ID = #{userId}")
    void clearSessionUserOwned(@Param("userId") @NotNull String userId);

    @Delete("TRUNCATE TABLE SESSION")
    void clearSession();

    @Select("SELECT * FROM SESSION")
    @Results(value = {
            @Result(property = "userId", column = "USER_ID")
    })
    @NotNull
    List<Session> findAllSession();

    @Select("SELECT * FROM SESSION WHERE ID = #{id}")
    @Results(value = {
            @Result(property = "userId", column = "USER_ID")
    })
    @Nullable
    Session findSessionById(@Param("id") @NotNull String id);

    @Delete("DELETE FROM SESSION WHERE ID = #{id}")
    void removeSession(@NotNull Session session);

    @Delete("DELETE FROM SESSION WHERE ID = #{id}")
    void removeSessionById(@Param("id") @NotNull String id);

    @Select("SELECT * FROM SESSION WHERE USER_ID = #{userId}")
    @Results(value = {
            @Result(property = "userId", column = "USER_ID")
    })
    @Nullable
    List<Session> findAllSessionUserOwned(@Param("userId") @NotNull String userId);

    @Select("SELECT * FROM SESSION WHERE USER_ID = #{userId} AND ID = #{id}")
    @Results(value = {
            @Result(property = "userId", column = "USER_ID")
    })
    @Nullable
    Session findSessionByIdUserOwned(@Param("userId") @NotNull String userId, @Param("id") @NotNull String id);

    @Delete("DELETE FROM SESSION WHERE ID = #{id} AND USER_ID = #{userId}")
    void removeSessionByIdUserOwned(@Param("userId") @NotNull String userId, @Param("id") @NotNull String id);

}
