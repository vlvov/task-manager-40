package ru.t1.vlvov.tm.api.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.vlvov.tm.model.Session;

import java.util.Collection;
import java.util.List;

public interface ISessionService {
    @NotNull
    @SneakyThrows
    Session add(@Nullable Session model);

    @NotNull
    @SneakyThrows
    Collection<Session> add(@Nullable Collection<Session> sessions);

    @NotNull
    @SneakyThrows
    Collection<Session> set(@Nullable Collection<Session> models);

    @SneakyThrows
    void clear();

    @NotNull
    @SneakyThrows
    List<Session> findAll();

    @NotNull
    @SneakyThrows
    Session remove(@Nullable Session session);

    @Nullable
    @SneakyThrows
    Session removeById(@Nullable String id);

    @Nullable
    @SneakyThrows
    Session findOneById(@Nullable String id);

    @SneakyThrows
    boolean existsById(@Nullable String id);

    @NotNull
    @SneakyThrows
    Session add(@Nullable String userId, @Nullable Session session);

    @SneakyThrows
    void clear(@Nullable String userId);

    @Nullable
    @SneakyThrows
    List<Session> findAll(@Nullable String userId);

    @Nullable
    @SneakyThrows
    Session findOneById(@Nullable String userId, @Nullable String id);

    @Nullable
    @SneakyThrows
    Session remove(@Nullable String userId, @Nullable Session session);

    @Nullable
    @SneakyThrows
    Session removeById(@Nullable String userId, @Nullable String id);

    @SneakyThrows
    boolean existsById(@Nullable String userId, @Nullable String id);
}
