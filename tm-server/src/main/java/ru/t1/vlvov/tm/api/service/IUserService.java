package ru.t1.vlvov.tm.api.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.vlvov.tm.enumerated.Role;
import ru.t1.vlvov.tm.model.User;

import java.util.Collection;
import java.util.List;

public interface IUserService {

    @Nullable
    User create(@Nullable String login, @Nullable String password);

    @Nullable
    User create(@Nullable String login, @Nullable String password, @Nullable String email);

    @Nullable
    User create(@Nullable String login, @Nullable String password, @Nullable Role role);

    @Nullable
    User findByLogin(@Nullable String login);

    @Nullable
    User findByEmail(@Nullable String email);

    @NotNull
    User removeByLogin(@Nullable String login);

    @Nullable
    User removeByEmail(@Nullable String email);

    @NotNull
    User setPassword(@Nullable String id, @Nullable String password);

    @NotNull
    User updateUser(@Nullable String id, @Nullable String firstName, @Nullable String lastName, @Nullable String middleName);

    boolean isLoginExist(@Nullable String login);

    boolean isEmailExist(@Nullable String email);

    @NotNull
    User lockUserByLogin(@Nullable String id);

    @NotNull
    User unlockUserByLogin(@Nullable String id);

    @NotNull
    @SneakyThrows
    User add(@Nullable User model);

    @NotNull
    @SneakyThrows
    Collection<User> add(@Nullable Collection<User> users);

    @NotNull
    @SneakyThrows
    Collection<User> set(@Nullable Collection<User> models);

    @SneakyThrows
    void clear();

    @NotNull
    @SneakyThrows
    List<User> findAll();

    @NotNull
    @SneakyThrows
    User remove(@Nullable User user);

    @Nullable
    @SneakyThrows
    User removeById(@Nullable String id);

    @Nullable
    @SneakyThrows
    User findOneById(@Nullable String id);

    @SneakyThrows
    boolean existsById(@Nullable String id);
}
