package ru.t1.vlvov.tm.dto.Response;

import lombok.NoArgsConstructor;
import ru.t1.vlvov.tm.model.User;

@NoArgsConstructor
public class UserProfileResponse extends AbstractUserResponse {

    public UserProfileResponse(User user) {
        super(user);
    }

}
